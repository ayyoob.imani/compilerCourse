// Define a grammar called Hello
grammar trycatch;


@parser::members {
 public int blockNumber=0;
 public int blockDepth=0;
}

//s : (T_plus | T_min | T_po | T_pc | T_intvar )*;
start : statement;
e : e T_plus t {int l = $T_plus.line;int p = $T_plus.pos;}| e T_min t {int l = $T_min.line;int p = $T_min.pos;}| t;
//e : t e1 | t;//ast
//e1 : T_plus t e1 | T_min t e1 | T_min t | T_plus t;
t : t T_star f {int l = $T_star.line;int p = $T_star.pos;}| t T_slash f {int l = $T_slash.line; int p = $T_slash.pos;}| t T_mode f {int l = $T_mode.line;int p = $T_mode.pos;}| f;
//t : f t1 | f;//ast
//t1 : T_star f t1| T_slash f t1| T_star f | T_mode f ;//ast some
f : T_po e T_pc {int l =$T_po.line;int p =$T_po.pos;int lp =$T_pc.line;int pl =$T_pc.pos;}| T_ID {int l =$T_ID.line;int p =$T_ID.pos;}| no;//
no: T_intvar {int l =$T_intvar.line;int p =$T_intvar.pos;}| T_floatvar {int l =$T_floatvar.line;int p =$T_floatvar.pos;}; //ast

masterCond : condition;
condition : condition T_or cond {int l =$T_or.line;int p =$T_or.pos;}| cond;
//condition : cond condition1 | cond;
//condition1 : T_or cond condition1 | T_or cond;
cond : cond T_and c {int l =$T_and.line;int p =$T_and.pos;}| c;
//cond : c cond1 | c;
//cond1 : T_and c cond1 | T_and c;
c : T_po condition T_pc {int l =$T_po.line;int p =$T_po.pos;int lp =$T_pc.line;int pl =$T_pc.pos;}| T_not condition {int l =$T_not.line;int p =$T_not.pos;} | bboolean;
bboolean : e relop e | e | T_true {int l =$T_true.line;int p =$T_true.pos;}| T_false {int l =$T_false.line;int p =$T_false.pos;};
relop : T_gt {int l =$T_gt.line;int p =$T_gt.pos;}|T_lt {int l =$T_lt.line;int p =$T_lt.pos;}|T_get {int l =$T_get.line;int p =$T_get.pos;}|T_let {int l =$T_let.line;int p =$T_let.pos;}|T_et {int l =$T_et.line;int p =$T_et.pos;}|T_net {int l =$T_net.line;int p =$T_net.pos;};


ifSt : ifPart   elsePart|ifPart ;
ifPart : T_if  condition   blockSt {int l=$T_if.line;int p=$T_if.pos;};
elsePart : T_else blockSt {int l=$T_else.line;int p=$T_else.pos;}| T_elif condition blockSt {int l=$T_elif.line;int p=$T_elif.pos;}|  T_elif condition blockSt elsePart {int l=$T_elif.line;int p=$T_elif.pos;};

//statement: statement  s | s ;
statement : s statement | s;
//statement1 : T_semicolon s statement1 | T_semicolon s;
s : ifSt | assignmentSt | declarSt | iOSt|T_ID addMinus T_semicolon {int l=$T_ID.line;int p=$T_ID.pos;int lp=$T_semicolon.line;int pp=$T_semicolon.pos;}| exceptionHandle;

blockSt : T_cbo statement T_cbc {int l=$T_cbo.line;int p=$T_cbo.pos;int lp=$T_cbc.line;int pp=$T_cbc.pos;}|T_cbo T_cbc {int l=$T_cbo.line;int p=$T_cbo.pos;int lp=$T_cbc.line;int pp=$T_cbc.pos;};

assignmentSt : T_ID T_as e T_semicolon {int l=$T_ID.line;int lp=$T_ID.pos;int p=$T_as.line;int pp=$T_as.pos;int pl=$T_semicolon.line;int ll=$T_semicolon.pos;}|T_ID T_as T_charvar T_semicolon {int l=$T_ID.line;int p=$T_ID.pos;int lp=$T_charvar.line;int pl=$T_charvar.pos;int pp=$T_semicolon.line;int ll=$T_semicolon.pos;}|T_ID T_as T_stringvar T_semicolon {int l=$T_ID.line;int p=$T_ID.pos;int lp=$T_as.line;int pl=$T_as.pos;int ll=$T_stringvar.line;int pp=$T_stringvar.pos;int lpp=$T_semicolon.line;int ppl=$T_semicolon.pos;};
 
declarSt : type T_ID T_semicolon {int l=$T_ID.line;int p=$T_ID.pos;int lp=$T_semicolon.line;int pl=$T_semicolon.pos;};
type:T_int {int l=$T_int.line;int p=$T_int.pos;}| T_float {int l=$T_float.line;int p=$T_float.pos;}| T_char {int l=$T_char.line;int p=$T_char.pos;}| T_string {int l=$T_string.line;int p=$T_string.pos;};
/*
iOSt: iO e T_semicolon | iO T_string T_semicolon;
iO : T_cin | T_cout;
*/
iOSt: inputSt | outputSt;
inputSt : T_cin T_ID T_semicolon {int l=$T_cin.line;int p=$T_cin.pos;int lp=$T_ID.line;int pl=$T_ID.pos;int ll=$T_semicolon.line;int pp=$T_semicolon.pos;};
outputSt :  T_cout T_stringvar T_semicolon {int l=$T_cout.line;int p=$T_cout.pos;int lp=$T_stringvar.line;int pl=$T_stringvar.pos;int ll=$T_semicolon.line;int pp=$T_semicolon.pos;} | T_cout e T_semicolon {int l=$T_cout.line;int p=$T_cout.pos;int lp=$T_semicolon.line;int pl=$T_semicolon.pos;};
addMinus : T_plusplus {int l=$T_plusplus.line;int p=$T_plusplus.pos;}| T_minusminus {int l=$T_minusminus.line;int p=$T_minusminus.pos;};

exceptionHandle : trySt catchSt ; 
trySt : T_try blockSt{int l=$T_try.line;int p=$T_try.pos;};
catchSt : T_catch T_po T_ID T_pc blockSt {int lp=$T_catch.line;int pl=$T_catch.pos;int ll=$T_po.line;int pp=$T_po.pos;int lpp=$T_ID.line;int ppl=$T_ID.pos;int llp=$T_pc.line;int pll=$T_pc.pos;};






T_plusplus : '++';
T_minusminus : '--';
T_plus : '+';
T_min : '-';
T_star : '*';
T_slash : '/';
T_mode: '%';
T_gt : '>';
T_lt : '<';
T_get : '>=';
T_let : '<=';
T_et : '==';
T_net : '!=';
T_as : '=';
T_and : '&&';
T_or : '||';
T_not : '!';
T_if : 'if';
T_else : 'else';
T_elif : 'elif';
T_po : '(';
T_pc : ')';
T_cbo : '{';
T_cbc : '}';
T_cin :'>>';
T_cout : '<<';
T_int : 'int';
T_intvar : ('0'..'9')+;
T_float : 'float';
T_floatvar : ('0'..'9')+'.'('0'..'9')+;
T_char : 'char';
T_true : 'true';
T_false : 'false';
T_charvar : '\''('a'..'z'|'A'..'Z')'\'';
T_string : 'string';
T_stringvar : '"'('\\"'|.)*?'"';
T_colon : ':';
T_comma : ',';
T_comment : '/*'.*?'*/' -> skip;
T_semicolon : ';';
T_throw : 'throw';
T_try : 'try';
T_catch: 'catch';
T_whitespace : [ \n\t\r]+ -> skip ;
T_eof: '<EOF>' -> skip;


T_ID : ('_'|'a'..'z'|'A'..'Z')('_'|'a'..'z'|'A'..'Z'|'0'..'9')*;



