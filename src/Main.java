// import org.antlr.runtime.CommonTokenStream;
// import org.antlr.runtime.TokenSource;
import org.antlr.v4.runtime.ANTLRFileStream;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.TokenStream;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.ParseTreeWalker;

//import MAG1Parser.StartContext;


public class Main {
	

    public static void main(String[] args) throws Exception {
        //String input = "hi";
        CharStream mstream = new ANTLRFileStream("c:\\android\\test.txt");

        MAG1Lexer lexer = new MAG1Lexer(mstream);
        TokenStream mstreaStream = new CommonTokenStream(lexer);
        MAG1Parser parser = new MAG1Parser(mstreaStream);
        //parser.getTrimParseTree();
        //parser.setBuildParseTree(true);
        //parser.addParseListener(new MAG1BaseListener());

        //String ff = parser.getTokenStream().getText();
        //CommonTree tree = (CommonTree) parser.javaSource().getTree();
        //DOTTreeGenerator gen = new DOTTreeGenerator();
        //StringTemplate st = gen.toDOT(tree);
        MAG1Parser.StartContext startContext = parser.start();
        ParseTreeWalker walker = new ParseTreeWalker();
        MAG1BaseListener listener = new MAG1BaseListener();
        walker.walk(listener, startContext);
        //ParseTree tree = parser.start();
        

       // System.out.println(tree.getText());

    }

}