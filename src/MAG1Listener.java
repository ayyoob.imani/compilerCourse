// Generated from C:\Enter here\documents\courses\91-92_2\Compiler\project\vandermonde\MAG1.g4 by ANTLR 4.0

import org.antlr.v4.runtime.tree.ParseTreeListener;

public interface MAG1Listener extends ParseTreeListener {

    void enterAssignmentSt(MAG1Parser.AssignmentStContext ctx);



    void exitAssignmentSt(MAG1Parser.AssignmentStContext ctx);



    void enterIOSt(MAG1Parser.IOStContext ctx);



    void exitIOSt(MAG1Parser.IOStContext ctx);



    void enterT1(MAG1Parser.T1Context ctx);



    void exitT1(MAG1Parser.T1Context ctx);



    void enterNo(MAG1Parser.NoContext ctx);



    void exitNo(MAG1Parser.NoContext ctx);



    void enterRelop(MAG1Parser.RelopContext ctx);



    void exitRelop(MAG1Parser.RelopContext ctx);



    void enterType(MAG1Parser.TypeContext ctx);



    void exitType(MAG1Parser.TypeContext ctx);



    void enterCond(MAG1Parser.CondContext ctx);



    void exitCond(MAG1Parser.CondContext ctx);



    void enterCond1(MAG1Parser.Cond1Context ctx);



    void exitCond1(MAG1Parser.Cond1Context ctx);



    void enterDeclarSt(MAG1Parser.DeclarStContext ctx);



    void exitDeclarSt(MAG1Parser.DeclarStContext ctx);



    void enterBboolean(MAG1Parser.BbooleanContext ctx);



    void exitBboolean(MAG1Parser.BbooleanContext ctx);



    void enterF(MAG1Parser.FContext ctx);



    void exitF(MAG1Parser.FContext ctx);



    void enterE(MAG1Parser.EContext ctx);



    void exitE(MAG1Parser.EContext ctx);



    void enterC(MAG1Parser.CContext ctx);



    void exitC(MAG1Parser.CContext ctx);



    void enterCondition(MAG1Parser.ConditionContext ctx);



    void exitCondition(MAG1Parser.ConditionContext ctx);



    void enterBlockSt(MAG1Parser.BlockStContext ctx);



    void exitBlockSt(MAG1Parser.BlockStContext ctx);



    void enterElsePart(MAG1Parser.ElsePartContext ctx);



    void exitElsePart(MAG1Parser.ElsePartContext ctx);



    void enterStatement(MAG1Parser.StatementContext ctx);



    void exitStatement(MAG1Parser.StatementContext ctx);



    void enterE1(MAG1Parser.E1Context ctx);



    void exitE1(MAG1Parser.E1Context ctx);



    void enterCondition1(MAG1Parser.Condition1Context ctx);



    void exitCondition1(MAG1Parser.Condition1Context ctx);



    void enterT(MAG1Parser.TContext ctx);



    void exitT(MAG1Parser.TContext ctx);



    void enterIfSt(MAG1Parser.IfStContext ctx);



    void exitIfSt(MAG1Parser.IfStContext ctx);



    void enterS(MAG1Parser.SContext ctx);



    void exitS(MAG1Parser.SContext ctx);



    void enterStart(MAG1Parser.StartContext ctx);



    void exitStart(MAG1Parser.StartContext ctx);



    void enterInputSt(MAG1Parser.InputStContext ctx);



    void exitInputSt(MAG1Parser.InputStContext ctx);



    void enterOutputSt(MAG1Parser.OutputStContext ctx);



    void exitOutputSt(MAG1Parser.OutputStContext ctx);

}