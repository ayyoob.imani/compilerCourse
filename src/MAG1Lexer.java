// Generated from C:\Enter here\documents\courses\91-92_2\Compiler\project\vandermonde\MAG1.g4 by ANTLR 4.0

import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.Lexer;
import org.antlr.v4.runtime.RuleContext;
import org.antlr.v4.runtime.atn.ATN;
import org.antlr.v4.runtime.atn.ATNSimulator;
import org.antlr.v4.runtime.atn.LexerATNSimulator;
import org.antlr.v4.runtime.atn.PredictionContextCache;
import org.antlr.v4.runtime.dfa.DFA;

@SuppressWarnings({ "all", "warnings", "unchecked", "unused", "cast" })
public class MAG1Lexer extends Lexer {

    protected static final DFA[]                  _decisionToDFA;
    protected static final PredictionContextCache _sharedContextCache =
                                                                              new PredictionContextCache();
    public static final int
                                                  T__1                = 1, T__0 = 2, T_plus = 3, T_min = 4, T_star = 5, T_slash = 6, T_mode = 7, T_gt = 8,
                                                                      T_lt = 9, T_get = 10, T_let = 11, T_et = 12, T_net = 13, T_as = 14, T_and = 15, T_or = 16,
                                                                      T_not = 17, T_if = 18, T_else = 19, T_elif = 20, T_po = 21, T_pc = 22, T_cbo = 23, T_cbc = 24,
                                                                      T_cin = 25, T_cout = 26, T_int = 27, T_intvar = 28, T_float = 29, T_floatvar = 30,
                                                                      T_char = 31, T_charvar = 32, T_string = 33, T_stringvar = 34, T_comment = 35, T_semicolon = 36,
                                                                      T_throw = 37, T_try = 38, T_catch = 39, T_whitespace = 40, T_ID = 41;
    public static String[]                        modeNames           = {
                                                                      "DEFAULT_MODE"
                                                                      };

    public static final String[]                  tokenNames          = {
                                                                      "<INVALID>",
                                                                      "'false'", "'true'", "'+'", "'-'", "'*'", "'/'", "'%'", "'>'", "'<'",
                                                                      "'>='", "'<='", "'=='", "'!='", "'='", "'&&'", "'||'", "'!'", "'if'",
                                                                      "'else'", "'elif'", "'('", "')'", "'{'", "'}'", "'>>'", "'<<'", "'int'",
                                                                      "T_intvar", "'float'", "T_floatvar", "'char'", "T_charvar", "'string'",
                                                                      "T_stringvar", "T_comment", "';'", "'throw'", "'try'", "'catch'", "T_whitespace",
                                                                      "T_ID"
                                                                      };
    public static final String[]                  ruleNames           = {
                                                                      "T__1", "T__0", "T_plus", "T_min", "T_star", "T_slash", "T_mode", "T_gt",
                                                                      "T_lt", "T_get", "T_let", "T_et", "T_net", "T_as", "T_and", "T_or", "T_not",
                                                                      "T_if", "T_else", "T_elif", "T_po", "T_pc", "T_cbo", "T_cbc", "T_cin",
                                                                      "T_cout", "T_int", "T_intvar", "T_float", "T_floatvar", "T_char", "T_charvar",
                                                                      "T_string", "T_stringvar", "T_comment", "T_semicolon", "T_throw", "T_try",
                                                                      "T_catch", "T_whitespace", "T_ID"
                                                                      };



    public MAG1Lexer(CharStream input) {
        super(input);
        _interp = new LexerATNSimulator(this, _ATN, _decisionToDFA, _sharedContextCache);
    }



    @Override
    public String getGrammarFileName() {
        return "MAG1.g4";
    }



    @Override
    public String[] getTokenNames() {
        return tokenNames;
    }



    @Override
    public String[] getRuleNames() {
        return ruleNames;
    }



    @Override
    public String[] getModeNames() {
        return modeNames;
    }



    @Override
    public ATN getATN() {
        return _ATN;
    }



    @Override
    public void action(RuleContext _localctx, int ruleIndex, int actionIndex) {
        switch (ruleIndex) {
            case 34:
                T_comment_action((RuleContext) _localctx, actionIndex);
                break;

            case 39:
                T_whitespace_action((RuleContext) _localctx, actionIndex);
                break;
        }
    }



    private void T_comment_action(RuleContext _localctx, int actionIndex) {
        switch (actionIndex) {
            case 0:
                skip();
                break;
        }
    }



    private void T_whitespace_action(RuleContext _localctx, int actionIndex) {
        switch (actionIndex) {
            case 1:
                skip();
                break;
        }
    }

    public static final String _serializedATN =
                                                      "\2\4+\u0102\b\1\4\2\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t" +
                                                              "\b\4\t\t\t\4\n\t\n\4\13\t\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20" +
                                                              "\t\20\4\21\t\21\4\22\t\22\4\23\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27" +
                                                              "\t\27\4\30\t\30\4\31\t\31\4\32\t\32\4\33\t\33\4\34\t\34\4\35\t\35\4\36" +
                                                              "\t\36\4\37\t\37\4 \t \4!\t!\4\"\t\"\4#\t#\4$\t$\4%\t%\4&\t&\4\'\t\'\4" +
                                                              "(\t(\4)\t)\4*\t*\3\2\3\2\3\2\3\2\3\2\3\2\3\3\3\3\3\3\3\3\3\3\3\4\3\4\3" +
                                                              "\5\3\5\3\6\3\6\3\7\3\7\3\b\3\b\3\t\3\t\3\n\3\n\3\13\3\13\3\13\3\f\3\f" +
                                                              "\3\f\3\r\3\r\3\r\3\16\3\16\3\16\3\17\3\17\3\20\3\20\3\20\3\21\3\21\3\21" +
                                                              "\3\22\3\22\3\23\3\23\3\23\3\24\3\24\3\24\3\24\3\24\3\25\3\25\3\25\3\25" +
                                                              "\3\25\3\26\3\26\3\27\3\27\3\30\3\30\3\31\3\31\3\32\3\32\3\32\3\33\3\33" +
                                                              "\3\33\3\34\3\34\3\34\3\34\3\35\6\35\u00a5\n\35\r\35\16\35\u00a6\3\36\3" +
                                                              "\36\3\36\3\36\3\36\3\36\3\37\6\37\u00b0\n\37\r\37\16\37\u00b1\3\37\3\37" +
                                                              "\6\37\u00b6\n\37\r\37\16\37\u00b7\3 \3 \3 \3 \3 \3!\3!\3!\3!\3\"\3\"\3" +
                                                              "\"\3\"\3\"\3\"\3\"\3#\3#\3#\3#\7#\u00ce\n#\f#\16#\u00d1\13#\3#\3#\3$\3" +
                                                              "$\3$\3$\7$\u00d9\n$\f$\16$\u00dc\13$\3$\3$\3$\3$\3$\3%\3%\3&\3&\3&\3&" +
                                                              "\3&\3&\3\'\3\'\3\'\3\'\3(\3(\3(\3(\3(\3(\3)\6)\u00f6\n)\r)\16)\u00f7\3" +
                                                              ")\3)\3*\3*\7*\u00fe\n*\f*\16*\u0101\13*\4\u00cf\u00da+\3\3\1\5\4\1\7\5" +
                                                              "\1\t\6\1\13\7\1\r\b\1\17\t\1\21\n\1\23\13\1\25\f\1\27\r\1\31\16\1\33\17" +
                                                              "\1\35\20\1\37\21\1!\22\1#\23\1%\24\1\'\25\1)\26\1+\27\1-\30\1/\31\1\61" +
                                                              "\32\1\63\33\1\65\34\1\67\35\19\36\1;\37\1= \1?!\1A\"\1C#\1E$\1G%\2I&\1" +
                                                              "K\'\1M(\1O)\1Q*\3S+\1\3\2\6\4C\\c|\5\13\f\17\17\"\"\5C\\aac|\6\62;C\\" +
                                                              "aac|\u0109\2\3\3\2\2\2\2\5\3\2\2\2\2\7\3\2\2\2\2\t\3\2\2\2\2\13\3\2\2" +
                                                              "\2\2\r\3\2\2\2\2\17\3\2\2\2\2\21\3\2\2\2\2\23\3\2\2\2\2\25\3\2\2\2\2\27" +
                                                              "\3\2\2\2\2\31\3\2\2\2\2\33\3\2\2\2\2\35\3\2\2\2\2\37\3\2\2\2\2!\3\2\2" +
                                                              "\2\2#\3\2\2\2\2%\3\2\2\2\2\'\3\2\2\2\2)\3\2\2\2\2+\3\2\2\2\2-\3\2\2\2" +
                                                              "\2/\3\2\2\2\2\61\3\2\2\2\2\63\3\2\2\2\2\65\3\2\2\2\2\67\3\2\2\2\29\3\2" +
                                                              "\2\2\2;\3\2\2\2\2=\3\2\2\2\2?\3\2\2\2\2A\3\2\2\2\2C\3\2\2\2\2E\3\2\2\2" +
                                                              "\2G\3\2\2\2\2I\3\2\2\2\2K\3\2\2\2\2M\3\2\2\2\2O\3\2\2\2\2Q\3\2\2\2\2S" +
                                                              "\3\2\2\2\3U\3\2\2\2\5[\3\2\2\2\7`\3\2\2\2\tb\3\2\2\2\13d\3\2\2\2\rf\3" +
                                                              "\2\2\2\17h\3\2\2\2\21j\3\2\2\2\23l\3\2\2\2\25n\3\2\2\2\27q\3\2\2\2\31" +
                                                              "t\3\2\2\2\33w\3\2\2\2\35z\3\2\2\2\37|\3\2\2\2!\177\3\2\2\2#\u0082\3\2" +
                                                              "\2\2%\u0084\3\2\2\2\'\u0087\3\2\2\2)\u008c\3\2\2\2+\u0091\3\2\2\2-\u0093" +
                                                              "\3\2\2\2/\u0095\3\2\2\2\61\u0097\3\2\2\2\63\u0099\3\2\2\2\65\u009c\3\2" +
                                                              "\2\2\67\u009f\3\2\2\29\u00a4\3\2\2\2;\u00a8\3\2\2\2=\u00af\3\2\2\2?\u00b9" +
                                                              "\3\2\2\2A\u00be\3\2\2\2C\u00c2\3\2\2\2E\u00c9\3\2\2\2G\u00d4\3\2\2\2I" +
                                                              "\u00e2\3\2\2\2K\u00e4\3\2\2\2M\u00ea\3\2\2\2O\u00ee\3\2\2\2Q\u00f5\3\2" +
                                                              "\2\2S\u00fb\3\2\2\2UV\7h\2\2VW\7c\2\2WX\7n\2\2XY\7u\2\2YZ\7g\2\2Z\4\3" +
                                                              "\2\2\2[\\\7v\2\2\\]\7t\2\2]^\7w\2\2^_\7g\2\2_\6\3\2\2\2`a\7-\2\2a\b\3" +
                                                              "\2\2\2bc\7/\2\2c\n\3\2\2\2de\7,\2\2e\f\3\2\2\2fg\7\61\2\2g\16\3\2\2\2" +
                                                              "hi\7\'\2\2i\20\3\2\2\2jk\7@\2\2k\22\3\2\2\2lm\7>\2\2m\24\3\2\2\2no\7@" +
                                                              "\2\2op\7?\2\2p\26\3\2\2\2qr\7>\2\2rs\7?\2\2s\30\3\2\2\2tu\7?\2\2uv\7?" +
                                                              "\2\2v\32\3\2\2\2wx\7#\2\2xy\7?\2\2y\34\3\2\2\2z{\7?\2\2{\36\3\2\2\2|}" +
                                                              "\7(\2\2}~\7(\2\2~ \3\2\2\2\177\u0080\7~\2\2\u0080\u0081\7~\2\2\u0081\"" +
                                                              "\3\2\2\2\u0082\u0083\7#\2\2\u0083$\3\2\2\2\u0084\u0085\7k\2\2\u0085\u0086" +
                                                              "\7h\2\2\u0086&\3\2\2\2\u0087\u0088\7g\2\2\u0088\u0089\7n\2\2\u0089\u008a" +
                                                              "\7u\2\2\u008a\u008b\7g\2\2\u008b(\3\2\2\2\u008c\u008d\7g\2\2\u008d\u008e" +
                                                              "\7n\2\2\u008e\u008f\7k\2\2\u008f\u0090\7h\2\2\u0090*\3\2\2\2\u0091\u0092" +
                                                              "\7*\2\2\u0092,\3\2\2\2\u0093\u0094\7+\2\2\u0094.\3\2\2\2\u0095\u0096\7" +
                                                              "}\2\2\u0096\60\3\2\2\2\u0097\u0098\7\177\2\2\u0098\62\3\2\2\2\u0099\u009a" +
                                                              "\7@\2\2\u009a\u009b\7@\2\2\u009b\64\3\2\2\2\u009c\u009d\7>\2\2\u009d\u009e" +
                                                              "\7>\2\2\u009e\66\3\2\2\2\u009f\u00a0\7k\2\2\u00a0\u00a1\7p\2\2\u00a1\u00a2" +
                                                              "\7v\2\2\u00a28\3\2\2\2\u00a3\u00a5\4\62;\2\u00a4\u00a3\3\2\2\2\u00a5\u00a6" +
                                                              "\3\2\2\2\u00a6\u00a4\3\2\2\2\u00a6\u00a7\3\2\2\2\u00a7:\3\2\2\2\u00a8" +
                                                              "\u00a9\7h\2\2\u00a9\u00aa\7n\2\2\u00aa\u00ab\7q\2\2\u00ab\u00ac\7c\2\2" +
                                                              "\u00ac\u00ad\7v\2\2\u00ad<\3\2\2\2\u00ae\u00b0\4\62;\2\u00af\u00ae\3\2" +
                                                              "\2\2\u00b0\u00b1\3\2\2\2\u00b1\u00af\3\2\2\2\u00b1\u00b2\3\2\2\2\u00b2" +
                                                              "\u00b3\3\2\2\2\u00b3\u00b5\7\60\2\2\u00b4\u00b6\4\62;\2\u00b5\u00b4\3" +
                                                              "\2\2\2\u00b6\u00b7\3\2\2\2\u00b7\u00b5\3\2\2\2\u00b7\u00b8\3\2\2\2\u00b8" +
                                                              ">\3\2\2\2\u00b9\u00ba\7e\2\2\u00ba\u00bb\7j\2\2\u00bb\u00bc\7c\2\2\u00bc" +
                                                              "\u00bd\7t\2\2\u00bd@\3\2\2\2\u00be\u00bf\7)\2\2\u00bf\u00c0\t\2\2\2\u00c0" +
                                                              "\u00c1\7)\2\2\u00c1B\3\2\2\2\u00c2\u00c3\7u\2\2\u00c3\u00c4\7v\2\2\u00c4" +
                                                              "\u00c5\7t\2\2\u00c5\u00c6\7k\2\2\u00c6\u00c7\7p\2\2\u00c7\u00c8\7i\2\2" +
                                                              "\u00c8D\3\2\2\2\u00c9\u00cf\7$\2\2\u00ca\u00cb\7^\2\2\u00cb\u00ce\7$\2" +
                                                              "\2\u00cc\u00ce\13\2\2\2\u00cd\u00ca\3\2\2\2\u00cd\u00cc\3\2\2\2\u00ce" +
                                                              "\u00d1\3\2\2\2\u00cf\u00d0\3\2\2\2\u00cf\u00cd\3\2\2\2\u00d0\u00d2\3\2" +
                                                              "\2\2\u00d1\u00cf\3\2\2\2\u00d2\u00d3\7$\2\2\u00d3F\3\2\2\2\u00d4\u00d5" +
                                                              "\7\61\2\2\u00d5\u00d6\7,\2\2\u00d6\u00da\3\2\2\2\u00d7\u00d9\13\2\2\2" +
                                                              "\u00d8\u00d7\3\2\2\2\u00d9\u00dc\3\2\2\2\u00da\u00db\3\2\2\2\u00da\u00d8" +
                                                              "\3\2\2\2\u00db\u00dd\3\2\2\2\u00dc\u00da\3\2\2\2\u00dd\u00de\7,\2\2\u00de" +
                                                              "\u00df\7\61\2\2\u00df\u00e0\3\2\2\2\u00e0\u00e1\b$\2\2\u00e1H\3\2\2\2" +
                                                              "\u00e2\u00e3\7=\2\2\u00e3J\3\2\2\2\u00e4\u00e5\7v\2\2\u00e5\u00e6\7j\2" +
                                                              "\2\u00e6\u00e7\7t\2\2\u00e7\u00e8\7q\2\2\u00e8\u00e9\7y\2\2\u00e9L\3\2" +
                                                              "\2\2\u00ea\u00eb\7v\2\2\u00eb\u00ec\7t\2\2\u00ec\u00ed\7{\2\2\u00edN\3" +
                                                              "\2\2\2\u00ee\u00ef\7e\2\2\u00ef\u00f0\7c\2\2\u00f0\u00f1\7v\2\2\u00f1" +
                                                              "\u00f2\7e\2\2\u00f2\u00f3\7j\2\2\u00f3P\3\2\2\2\u00f4\u00f6\t\3\2\2\u00f5" +
                                                              "\u00f4\3\2\2\2\u00f6\u00f7\3\2\2\2\u00f7\u00f5\3\2\2\2\u00f7\u00f8\3\2" +
                                                              "\2\2\u00f8\u00f9\3\2\2\2\u00f9\u00fa\b)\3\2\u00faR\3\2\2\2\u00fb\u00ff" +
                                                              "\t\4\2\2\u00fc\u00fe\t\5\2\2\u00fd\u00fc\3\2\2\2\u00fe\u0101\3\2\2\2\u00ff" +
                                                              "\u00fd\3\2\2\2\u00ff\u0100\3\2\2\2\u0100T\3\2\2\2\u0101\u00ff\3\2\2\2" +
                                                              "\13\2\u00a6\u00b1\u00b7\u00cd\u00cf\u00da\u00f7\u00ff";
    public static final ATN    _ATN           =
                                                      ATNSimulator.deserialize(_serializedATN.toCharArray());
    static {
        _decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
    }
}